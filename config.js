
module.exports = {
  auth: {
    clientID: 'red-board',
    clientSecret: "xxx",
    callbackURL: 'http://localhost:1880/playground/auth/strategy/callback',
    logoutURL: 'http://localhost:1880/playground'
  },
  credentialSecret: 'xxx',
  admins: [ 'mdonze', 'sfargier' ]
}