FROM node:lts-slim

WORKDIR /app
ADD . /app
ADD package.json /root/.node-red/

RUN apt-get update \
	&& apt-get install -y apt-transport-https ca-certificates \
	&& echo 'deb [trusted=yes] https://mro-dev.web.cern.ch/distfiles/debian/stretch mro-main/' >> /etc/apt/sources.list \
	&& apt-get update \
	&& apt-get install -y cmw-core libcurl4-nss-dev git \
	&& echo "search cern.ch" >> /etc/resolv.conf \
	&& mkdir -p /user \
	&& npm install pm2 && mkdir .pm2 && chmod 777 .pm2

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:node_modules/.bin
ENV HOME=/app

#Add RBAC certificates
ADD rbac.tar /user/

EXPOSE 1880/tcp

CMD pm2 --no-daemon start -o /dev/null -e /dev/null --no-autorestart ./node_modules/.bin/node-red -- -s ./settings.js
