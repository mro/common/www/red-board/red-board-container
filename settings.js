
const { get, includes, isNil } = require('lodash');
const { Issuer, Strategy } = require('openid-client');

var config;
try {
  config = require('/etc/app/config');
}
catch (e) {
  config = require('./config');
}

async function createAdminAuth() {
  const oidcIssuer =
    await Issuer.discover('https://auth.cern.ch/auth/realms/cern');
  const client = new oidcIssuer.Client({
    client_id: get(config, 'auth.clientID'), /* eslint-disable-line camelcase */
    client_secret: get(config, 'auth.clientSecret'), /* eslint-disable-line camelcase */
    redirect_uris: [ get(config, 'auth.callbackURL') ], /* eslint-disable-line camelcase */
    post_logout_redirect_uris: [ get(config, 'auth.logoutURL') ] /* eslint-disable-line camelcase */
  });
  var users = {};

  return {
    type: 'strategy',
    strategy: {
      name: 'auth.cern.ch',
      label: 'Sign-in with CERN',
      icon: 'fa-unlock',
      strategy: Strategy,
      options: {
        client,
        verify: /** @type {import('openid-client').StrategyVerifyCallbackUserInfo<any>} */
          function(tokenSet, done) {
            const info = tokenSet.claims();
            const username = get(info, 'preferred_username');
            console.log('user authenticated:', username);

            const isAdmin = (config.admins) ?
              includes(get(config, 'admins'), username) :
              includes(get(info, 'cern_roles'), 'editor');

            users[username] = isAdmin ? [ '*' ] : [ 'read', 'inject.write', 'debug.write' ];
            done(null, username);
        }
      }
    },
    users: async function(username) {
      const perms = users[username];
      if (isNil(perms)) {
        return null;
      }
      return { username, permissions: perms };
    }
  };
}

const settings = {
    // the tcp port that the Node-RED web server is listening on
    uiPort: process.env.PORT || 1880,
    mqttReconnectTime: 15000,
    serialReconnectTime: 15000,
    debugMaxLength: 1000,
    debugUseColors: true,
    
    // The file containing the flows. If not set, it defaults to flows_<hostname>.json
    flowFile: 'flows.json',

    // To enabled pretty-printing of the flow within the flow file, set the following
    //  property to true:
    flowFilePretty: true,

    credentialSecret: get(config, 'credentialSecret', 'a-secret-key'),

    //userDir: '/home/nol/.node-red/',
    //nodesDir: '/home/nol/.node-red/nodes',
    //httpNodeRoot: '/red-nodes',

    httpRoot: get(config, 'basePath', '/'),

    ui: { path: "ui" },

    //requireHttps: true,
    https: async function() {
      /* async adminAuth hack */
      settings.adminAuth = await createAdminAuth();
      return undefined;
    },
    // uncomment to disable editor
    // disableEditor: true,
    // httpAdminRoot: false,

    functionGlobalContext: {
    },
    exportGlobalContextKeys: true,
    //contextStorage: {
    //    default: {
    //        module:"localfilesystem"
    //    },
    //},
    paletteCategories: ['dashboard', 'network', 'subflows', 'common', 'function', 'sequence', 'parser', 'storage'],

    // Configure the logging output
    logging: {
        console: {
            level: "info",
            metrics: false,
            audit: false
        }
    },

    // Customising the editor
    editorTheme: {
        projects: {
            enabled: true
        }
    }
}

module.exports = settings;