# Flow-project
This project is a demo of using [Node-red](https://nodered.org/) as a dashboard solution usable in CERN control infrastructure.

 # Docker image
 In order to use the Docker image build with the provided *Dockerfile*, you may use this Docker command line:
`docker run --dns-search cern.ch -p 1880:1880 node-red-rda:1.0`
 
 The *--dns-search* allows RDA3 to lookup RDA servers (server names are without *.cern.ch* suffix).
 
*Node-red* application will be exposed on TCP port 1880. Simply point your browser to http://localhost:1880 to start using the running Docker container.

*Node-red* settings are stored in /root/.node-red folder. You can map it to a local storage.

This Docker image contains [rda3-client](https://gitlab.cern.ch/apc/common/www/node-red/rda3-client) plugin and the [node-red-dashboard](https://flows.nodered.org/node/node-red-dashboard) plugin.

